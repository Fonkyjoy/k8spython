Sources de l'application
https://gitlab.com/cdufour1/luminess-projet1


image : opusidea/luminess_projet1

---------------------------
*Commande*:

**Création d’un déploiement** => 
kubectl create deployment hello-minikube --image=docker.io/nginx:1.23
kubectl expose deployment hello-minikube --type=NodePort --port=80 

**ressource**
Les ressources peuvent etre de type:
    - pod (po)
    - namespace (ns)
    - service ()
    -replicaset (rs)
Création d'une ressource => kubectl apply -f nom_du_fichier.yaml
Supprésion d'une ressource => kubectl delete -f nom_du_fichier.yaml
etat de la ressource => kubectl get ressource
logs ressource =>  kubectl logs ressource/nomDeLaRessource
Description d’une ressource => kubectl describe ressource/nomDeLaRessource

**pod**
Visualisation des noeuds => kubectl get pod (po) 
si le noeud n'est pas visible cela peut venir du namespace
dans le context.
visualisation d es logs des noeuds dans le namespace => kubectl logs nomDuPoD -n nomDuNamespace
Visualisation des status => minikube status 

**service**
sert de passerelle entre deux pods


**deploy**
Visualisation des deploiements => Kubectl get deploy 
Supprimer un deploiements => Kubectl delete deploy/nom_du_deploiement 

** service**
Visualisation des services => Kubectl get service (option –o wide pour avoir plus d’information) 

**commande linux** 
Execution de commande linux dans le contenaire => kubectl exec pod/contenaire -- commande 

**shell inversé** 
Pour passer en shell inversé  => kubectl exec -it pod/ contenaire -- sh 
Pour vérifier si le service est ok en shell inversé => curl htpp://nom_du_service:port 